class FileProcessor:
    MAX_PEOPLE_NUM = 20

    def __init__(self, file_name):
        """Constructor"""
        self.current_line = 1
        try:
            self.file = open(file_name, 'r', encoding='utf-8')
        except IOError:
            raise IOError("File \"%s\" failed to open" % file_name)

    def read_task(self):
        task_volume_line = self.file.readline()
        task_volume = int(task_volume_line)
        if task_volume < 0 or task_volume > self.MAX_PEOPLE_NUM:
            raise Exception("Wrong format in line %i. Integer from 0 to %i expected, \"%s\" given" %
                            (self.current_line, self.MAX_PEOPLE_NUM, task_volume_line[0:-1]))

        self.current_line += 1
        task_units = {}
        for command_pack in range(task_volume):
            task_units.update({self.current_line: self.file.readline()})
            self.current_line += 1

        return task_units
