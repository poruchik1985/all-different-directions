import math
from functools import reduce

from WrongFormatException import WrongFormatException


class Task:
    START_COMMAND = "start"
    COMMON_COMMANDS = {
        'turn': lambda state, v:
        {
            "x": state["x"],
            "y": state["y"],
            "ang": state["ang"] + v
        },
        'walk': lambda state, v:
        {
            "x": state["x"] + (v * math.cos(math.radians(state["ang"]))),
            "y": state["y"] + (v * math.sin(math.radians(state["ang"]))),
            "ang": state["ang"]
        }
    }

    def __init__(self, task_units):
        """Constructor"""
        final_task_states = []
        for unit_key in task_units:
            raw_unit = task_units[unit_key].split()
            if len(raw_unit) < 4 or len(raw_unit) % 2 != 0:
                raise WrongFormatException(unit_key,
                                           self.START_COMMAND,
                                           self.COMMON_COMMANDS,
                                           task_units[unit_key][0:-1])

            state = {
                "x": float(raw_unit[0]),
                "y": float(raw_unit[1]),
                "ang": float(raw_unit[3])
            }
            command = raw_unit[2]

            if type(state["x"]) != float or \
                    type(state["y"]) != float or \
                    command != self.START_COMMAND or \
                    type(state["ang"]) != float:
                raise WrongFormatException(unit_key,
                                           self.START_COMMAND,
                                           self.COMMON_COMMANDS,
                                           task_units[unit_key][0:-1])

            for command_index in range(4, len(raw_unit), 2):
                if raw_unit[command_index] not in list(self.COMMON_COMMANDS.keys()) or \
                        type(float(raw_unit[command_index + 1])) != float:
                    raise WrongFormatException(unit_key,
                                               self.START_COMMAND,
                                               self.COMMON_COMMANDS,
                                               task_units[unit_key][0:-1])

                state = self.COMMON_COMMANDS[raw_unit[command_index]](state, float(raw_unit[command_index + 1]))

            final_task_states.append(state)

        self.average_point = {
            "x": reduce(lambda state_1, state_2: state_1 + state_2,
                        map(lambda state_x: state_x["x"], final_task_states)) / len(final_task_states),
            "y": reduce(lambda state_1, state_2: state_1 + state_2,
                        map(lambda state_y: state_y["y"], final_task_states)) / len(final_task_states)
        }

        self.worst_disnance = reduce(lambda state_1, state_2: state_1 if state_1 > state_2 else state_2,
                                     map(lambda state_dist:
                                         math.sqrt(
                                             (state_dist["x"] - self.average_point["x"]) ** 2 +
                                             (state_dist["y"] - self.average_point["y"]) ** 2
                                         ),
                                         final_task_states))
