class WrongFormatException(Exception):
    def __init__(self, line, start_command_expected, common_commands_expected, given):
        self.line = line
        self.start_command_expected = start_command_expected
        self.common_commands_expected = common_commands_expected
        self.given = given

    def __str__(self):
        return repr("Wrong format in line %i. "
                    "<f> <f> <cs> <cv> <cn> <cv> [(<cn> <cv>)*], where "
                    "<f> - float with precision up to 4 decimal places, "
                    "<cs> - the %s command from, "
                    "<cn> - a command from list %s, "
                    "<cv> - float command value, expected, \"%s\" given" %
                    (self.line, self.start_command_expected, self.common_commands_expected, self.given))
