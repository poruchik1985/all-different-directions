import sys
from FileProcessor import FileProcessor
from Task import Task

if __name__ == "__main__":
    if len(sys.argv) < 2:
        raise Exception("File name must be specified.")

    output = open("output.txt", 'w', encoding='utf-8')

    processor = FileProcessor(sys.argv[1])

    while True:
        task_units = processor.read_task()
        if len(task_units) == 0:
            break

        task = Task(task_units)

        output.write("%f %f %f\n" % (task.average_point["x"], task.average_point["y"], task.worst_disnance))
